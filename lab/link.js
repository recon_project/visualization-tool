class Link {
    constructor(origin,destination,type){
        this.origin=origin;
        this.destination=destination;
        this.type=type;

    }

    display(){
        stroke(0);
        line(this.origin.x,this.origin.y,this.destination.x,this.destination.y);
    }
}