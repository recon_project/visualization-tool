
function orthogonalProjection1(a, b, p) {
  
    // find nearest point along a LINE
    
    d1 = p5.Vector.sub(b, a).normalize()
    d2 = p5.Vector.sub(p, a)
    
    d1.mult(d2.dot(d1))
      
    return p5.Vector.add(a, d1)
    
  }
    
    
  function orthogonalProjection2(a, b, p) {
    
    // find nearest point alont a SEGMENT 
    
    d1 = p5.Vector.sub(b, a);
    d2 = p5.Vector.sub(p, a);
    l1 = d1.mag();
    
    dotp = constrain(d2.dot(d1.normalize()), 0, l1);
        
    return p5.Vector.add(a, d1.mult(dotp))
    
  }

  // function getConstraintOffset(anchor){
  //   anchorPole=anchor.anchorPos;
  //   switch(anchorPole){
  //     ''
  //   }
  // }