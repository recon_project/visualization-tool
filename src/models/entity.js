// Entity class : a combination of a p5.js shape + a matter.js body
// can be extended with a shape
// if not extended, it's a text without a shape around
// can behold an "innerSet" for future zooming capability in a new set of entities within an Entity

class Entity {
    constructor(pos,value,options,stroke,type,id=0){
        this.pos=pos;
        this.id=id;
        this.stroke=stroke;
        this.value=value;
        this.options=options;
        this.anchorSize=5;
    }
    setId(id){
        this.id=id;
    }
    // saveAsJSON(){
    //     save(this,this.id+'-entity.json');
    // }
    display(){
        var textPos;
        if(this.isDragging){
            stroke(12,323,12);
            strokeWeight(6);
        }else{
            fill(255);
            stroke(color(this.stroke.color));
            strokeWeight(1);
        }
        push();
        rectMode(CENTER);
        noFill();
        noStroke();
        
        rect(this.body.position.x,this.body.position.y,this.width,this.height);
        textPos=createVector(this.body.position.x,this.body.position.y+4);
        fill(0);
        textSize(16/currentScale);
        textAlign(CENTER);
        strokeWeight(0);
        text(this.value,textPos.x,textPos.y);
        this.pos=this.body.position;
        pop();
    }

    checkClick(absMouseX,absMouseY,click){

    }
}
