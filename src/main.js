


function toggleModal(type) {
  noLoop();
  var modal = document.querySelector(".modal");
  var textArea = modal.querySelector('textarea');
  textArea.value =  '';
  const buttonTextArea=document.querySelector('button.submit-textarea');
  buttonTextArea.setAttribute('data-type',type);
  modal.classList.toggle("show-modal");
  modal.querySelector('textarea').focus();
}



var menuLink;
document.addEventListener('DOMContentLoaded', (e) => {
  
  const closeButton = document.querySelector(".close-button");
  closeButton.addEventListener("click", toggleModal);
  
  const buttonTextArea=document.querySelector('button.submit-textarea');
  buttonTextArea.addEventListener('click',(e)=>{
    var modal = document.querySelector(".modal");
    
    console.log( document.getElementById("textarea").value);
    var text = document.getElementById("textarea").value;
    let type = buttonTextArea.getAttribute('data-type');
    console.log(menuRectangle);
    if(text!=null){
      switch(type){
        case 'new':
          inputValue=text;
          break;
        case 'rectangle':
          menuRectangle.contextTarget.value = text;
        break;
        case 'ellipse':
          menuEllipse.contextTarget.value = text;
        break;
        case 'link':
          menuLink.contextTarget.value = text;
        break;

      }
      inputValue=text;
    } 
    loop();
    modal.classList.toggle('show-modal');

  });

  const $inputFile = document.querySelector('#file-input');
  $inputFile.value = '';

  const $toolPanel = document.querySelector('#tool-panel');

  let $tools = $toolPanel.querySelectorAll('.tool-group .tool-item button');
  console.log($tools);
  $tools.forEach($tool => {
    var dataTool = $tool.getAttribute('data-tool');
    $tool.addEventListener('click', (e) => {
      console.log(dataTool);
      switch (dataTool) {
        case 'rectangle':
          rectangleTool();
          break;
        case 'ellipse':
          ellipseTool();
          break;
        case 'link':
          linkTool();
          break;
        case 'save':
          const path = $tool.getAttribute('data-path');
          console.log(path);
          saveTool(path);
          break;
        // case 'load':
        case 'clear':
          clearTool();
          break;


      }
    });
  });

  // https://github.com/m-thalmann/contextmenujs
  var menuLinkItems = [
    {
      "text": "Delete link",
      "icon": "&#10006;",
      "events": {
        "click": function (e) {
          console.log('delete');
          console.log(menuLink.contextTarget.id);
          mainSet.deleteLink(menuLink.contextTarget.id);
        }
      }
    },
    // {
    //     "type": ContextMenu.DIVIDER              
    //   },
    {
      "text": "Rename link",
      "events": {
        "click": function (e) {
          console.log("clicked");
          console.log(menuLink.contextTarget);
          toggleModal('link');
        },
        "mouseover": function (e) {
          console.log("mouse is over menuitem");
        }
      }
    },
    {
      "text": "Resize / Customize",
      "sub": [
        {
          "text": "Change stroke color",
          "enabled": true,
          "events": {
            "click": function (e) {
              console.log('clicked stroke color');
              openColorPicker('stroke','link');
            }
          }
        },
        {
          "text": "Resize",
          "enabled": true,
          "events": {
            "click": function (e) {

              resizeLink(e.clientX, e.clientY);
            }
          }
        }
      ]
    }
  ];
  menuLink = new ContextMenu(menuLinkItems);
  //   document.addEventListener("contextmenu", function(e){
  //     menuLink.display(e);
  //   });

  var menuRectangleItems = [
    {
      "text": "Delete entity",
      "icon": "&#10006;",
      "events": {
        "click": function (e) {
          // console.log('delete');
          // console.log(menuRectangle.contextTarget.id);
          mainSet.deleteEntity(menuRectangle.contextTarget.id);
        }
      }
    },
    // {
    //     "type": ContextMenu.DIVIDER              // This item is a divider (shows only gray line, no text etc.)
    //   },
    {
      "text": "Rename Entity",
      "events": {                              // Adds eventlisteners to the item (you can use any event there is)
        "click": function (e) {
          // console.log("clicked");
          // console.log(menuRectangle.contextTarget);
          toggleModal('rectangle');
          // if(inputValue!=null)
          //   menuRectangle.contextTarget.value = inputValue;
        },
        "mouseover": function (e) {
          // console.log("mouse is over menuitem");
        }
      }
    },
    {
      "text": "Resize / Customize",
      "sub": [
        {
          "text": "Change stroke color",
          "enabled": true,
          "events": {
            "click": function (e) {
              // console.log('clicked stroke color');
              openColorPicker('stroke','rectangle');
            }
          }
        },
        {
          "text": "Change fill color",
          "enabled": true,
          "events": {
            "click": function (e) {
              openColorPicker('fill','rectangle');
            }
          }
        }
      ]
    }
  ];
  menuRectangle = new ContextMenu(menuRectangleItems);

  var menuEllipseItems = [
    {
      "text": "Delete entity",
      "icon": "&#10006;",
      "events": {
        "click": function (e) {
          // console.log('delete');
          // console.log(menuEllipse.contextTarget.id);
          mainSet.deleteEntity(menuEllipse.contextTarget.id);
        }
      },


    },
    // {
    //     "type": ContextMenu.DIVIDER              // This item is a divider (shows only gray line, no text etc.)
    //   },
    {
      "text": "Rename Entity",
      "events": {                              // Adds eventlisteners to the item (you can use any event there is)
        "click": function (e) {
          // console.log("clicked");
          // console.log(menuEllipse.contextTarget);
          toggleModal('ellipse');
        },
        "mouseover": function (e) {
          // console.log("mouse is over menuitem");
        }
      }
    },
    {
      "text": "Resize / Customize",
      "sub": [
        {
          "text": "Change stroke color",
          "enabled": true,
          "events": {
            "click": function (e) {
              // console.log('clicked stroke color');
              openColorPicker('stroke','ellipse');
            }
          }
        },
        {
          "text": "Change fill color",
          "enabled": true,
          "events": {
            "click": function (e) {
              // console.log('clicked fill color');
              openColorPicker('fill','ellipse');
            }
          }
        }
      ]
    }
  ];
  menuEllipse = new ContextMenu(menuEllipseItems);










});