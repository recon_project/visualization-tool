class Link{
    constructor(node1, node2, constraint,stroke,value, id=null){
        this.constraint=constraint;
        this.stroke=stroke;
        this.value=value;
        this.node1=node1;
        this.node2=node2;
        if(id!=null)
            this.id=id;
        // console.log(this.constraint);
        if(this.constraint!=null)
            World.add(world, constraint);
    }

    setId(id){
        this.id=id;
    }
    // saveAsJSON(){
    //     save(this,this.id+'-link.json');
    // }

    exportJSON(){
        jsonData=
            {
                stroke:this.stroke,
                value:this.value,
                node1:this.node1,
                node2:this.node2,
                constraint: {
                    bodyA:this.constraint.bodyA.id,
                    bodyB:this.constraint.bodyB.id,
                    
                }
            }
        return jsonData;
    }
    toJSON(){
        return Object.assign(new Link(), JSON.stringify(this, (key, value) => {
            if (typeof value === 'object' && value !== null) {
              // Duplicate reference found, discard key
              if (cache.includes(value)) return;
          
              // Store value in our collection
              cache.push(value);
            }
            return value;
        }));
    }
    display(){
        push();
        // stroke(this.stroke.levels);
        // console.log(this.stroke.color);
        stroke(color(this.stroke.color));
        let posA=Constraint.pointAWorld(this.constraint);
        let posB=Constraint.pointBWorld(this.constraint);
        // let posB=this.constraint.bodyB.position;
        line(posA.x,posA.y,posB.x,posB.y);
        let center=createVector((posA.x+posB.x)/2,(posA.y+posB.y)/2);
        noStroke();
        textSize(16/currentScale);
        text(this.value,center.x,center.y);
        this.constraint.position;
        pop();
    }
    checkClick(absMouseX,absMouseY,click){
            // console.log('link click var : '+click);
            if(!entityDragging){
                // console.log('we explore a constraint');
                // console.log(element);
                let constraint = this.constraint;
                let p = createVector(absMouseX, absMouseY);
                // console.log(transformX,transformY);
                let pointA = Constraint.pointAWorld(constraint);
                let pointB = Constraint.pointBWorld(constraint);
    
    
                let a = createVector(pointA.x, pointA.y);
                let b = createVector(pointB.x, pointB.y);
    
                let op = orthogonalProjection2(a, b, p);
                let distance = p5.Vector.dist(p, op);
                if (distance < 20) {
                    isPanning = false;
                    // console.log('click area in the link : '+click);
                    if(click=='left'){
                        // console.log('clicked on a link : ');
                        // console.log(this);
                        // to create
                        linkTextTool(this);
                    }else if(click=='right'){
                        // console.log('menu link');
                        menuLink.display(event,this);
                    }
                    return true;
                }
            }
            return false;
       
    }

}