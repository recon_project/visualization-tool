// contains multiple entities
// in the future we could zoom in an entity and discover an "innerSet" : a new point of view in the canvas
// the Entity class already beholds an unused innerSet attribute  
class Set{
    constructor(entities,links){
        this.links=links;
        this.entities=entities;
        this.initLinksID();
        // this.initEntitiesID();

    }
    initLinksID(){
        for (let i = 0; i < this.links.length; i++) {
            const element = this.links[i];
            element.setId(i);
        }
    }
    initEntitiesID(){
        for (let i = 0; i < this.entities.length; i++) {
            const element = this.entities[i];
            element.setId(i);
        }
    }
    //each time we change the attributes of an object, we have to update this function accordingly
    importJSON(jsonData){
        //assign entities
        jsonData.entities.forEach((entity,key)=>{
            // console.log(entity);
            // console.log(key);
            let newEntity;
            // console.log(entity.type);
            switch(entity.type){
                case 'Rectangle':
                    newEntity=new Rectangle(entity.pos,entity.value,entity.options,entity.width,entity.height,entity.stroke,entity.fill,key);
                    break;
                case 'Ellipse':
                newEntity=new Ellipse(entity.pos,entity.value,entity.options,entity.diameter,entity.stroke,entity.fill,key);
                    break;
            }
            // console.log(newEntity);
            mainSet.addEntity(newEntity);
           
        })
        let newLink;

        jsonData.links.forEach((link,key)=>{
            // console.log(link);
            let node1=this.entities.find(entity=>{
                return entity.id===link.node1;
            });
            let node2=this.entities.find(entity=>{
                // console.log(entity.id,link.node2);
                return entity.id===link.node2;
            });
            let constraintOption={
                bodyA: node1.body,
                bodyB: node2.body,
                pointA: link.constraint.pointA,
                pointB: link.constraint.pointB,
              length: link.constraint.length,
              stiffness: link.constraint.stiffness
            }
            let constraint=Constraint.create(constraintOption);

            console.log(link.stroke);
            newLink=new Link(link.node1,link.node2,constraint,link.stroke,link.value,key);    
            mainSet.addLink(newLink);
        })
        
        
        //assign links
        
    }
    
    // import / export json data
// should be a Set class function 
    //each time we change the attributes of an object, we have to update this function accordingly

    saveAsJSON(){
      let data={
        'entities':[],
        'links':[]
      };
  
      this.entities.forEach(entity=> {
        data.entities.push({
          pos:entity.pos,
          value:entity.value,
          options:entity.options,
          // - [ ] calculate width in function of value.length
          width:entity.width,
          height:entity.height,
          diameter:entity.diameter,
          stroke:entity.stroke,
          fill:entity.fill,
          type:entity.constructor.name,
          innerSet:entity.innerSet,
          anchorSize:entity.anchorSize,
          id:entity.id
        })
        
      });
      this.links.forEach(link=> {
        data.links.push({
          id:link.id,
          constraint: {
            length:link.constraint.length,
            stiffness:link.constraint.stiffness,
            pointA:link.constraint.pointA,
            pointB:link.constraint.pointB
          },
          stroke:link.stroke,
          value:link.value,
          // stroke:entity.stroke,
          // [ ] add bodysource / bodydest
          node1:link.node1,
          node2:link.node2
        })
      });
      return (data);
    
  }
  

    display(){
        this.links.forEach(link => {
            link.display();
          });
        this.entities.forEach(element=>{
            element.display();
          });
    }
    saveSet(){

    }
    loadSet(){

    }
    addEntity(entity) {
        if(this.entities.length>0){
            let maxIdLink=this.entities.reduce((prev,current)=>{
                return (prev.id>current.id)?prev:current;
            });
            entity.setId(maxIdLink.id+1);
        }else{
            entity.setId(0);
        }
        this.entities.push(entity);
    }
    deleteEntity(id){
        console.log(this.entities);
        let entity=this.entities.find((element)=>{
            console.log(id);
            console.log(element.id);
            return element.id==id;
        });
        console.log(entity);
        Matter.Composite.remove(world,entity.body);
        // - [ ] also remove all related links
        let foundFilters=this.links.filter(link=>{
            console.log(link);
            return ((link.node1===id)||(link.node2===id));
        });
        console.log(foundFilters);
        foundFilters.forEach(filter=>{
            mainSet.deleteLink(filter.id);
        })
        this.entities=this.entities.filter((entity)=>{
            return entity.id!==id;
        })
    }
    addLink(link){
        if(this.links.length>0){
            let maxIdLink=this.links.reduce((prev,current)=>{
                return (prev.id>current.id)?prev:current;
            });
            link.setId(maxIdLink.id+1);
        }else{
            link.setId(0);
        }

        // do we need this bi-directional relation ? because each time we delete a link, we have to loop on entities
/*         let bodyAID=link.linkedBodyA;
        let bodyBID=link.linkedBodyB;
        let entityA = this.entities.find(entity=>{
            return entity.id==bodyAID;
        })
        let entityB = this.entities.find(entity=>{
            return entity.id==bodyBID;
        })
        entityA.relatedLinks.push(link.id);
        entityB.relatedLinks.push(link.id); */

        this.links.push(link);
    }
    deleteLink(id){
        console.log(this.links);
        let linksArray=this.links;
        let link=linksArray.find((element)=>{
            return element.id===id;
        });
        console.log(link);
        Matter.Composite.remove(world,link.constraint);
        this.links=this.links.filter((link)=>{
            return link.id!==id;
        })
    }
    resetDragging(){
        this.entities.forEach(element=>{
            element.isDragging=false;
        })
    }
    
    checkMouseClick(absMouseX, absMouseY, click) {
        let found=false;
        [...this.entities, ...this.links].forEach(element => {
            found=element.checkClick(absMouseX,absMouseY, click);
            if(found)return;
        });
    }

    updateEntityDragPos(absMouseX,absMouseY){
        this.entities.forEach(element=>{  
            if(element.isDragging){
              element.body.position.x=absMouseX;
              element.body.position.y=absMouseY;
            }
          });
    }
    seeAnchors(){
        // console.log(this.entities);
        this.entities.forEach(entity=>{
            if(entity.constructor.name=='Rectangle' || entity.constructor.name=='Ellipse')
                entity.revealAnchors();
        });
    }
    checkAnchorsClick(absMouseX,absMouseY){
        this.entities.forEach(entity=>{
            if(entity.constructor.name!='Entity'){
                entity.checkAnchorsClick(absMouseX,absMouseY);

            }
        });
    }

}