var Engine = Matter.Engine,
  // Render = Matter.Render,

  World = Matter.World,
  Bodies = Matter.Bodies,
  Constraint = Matter.Constraint,
  Runner = Matter.Runner;

var engine;
var world;
var ground;
var runner;


function setup() {
    var mainCanvas=createCanvas(windowWidth, windowHeight);

    // var mainCanvas=createCanvas(600, 600);
    mainCanvas.parent("canvas");
    engine = Engine.create();
    world = engine.world;
    
    var options={
      friction:0,
      restitution:0.95,
      isStatic:true
    }
    box1 = Bodies.rectangle(200, 100, 80, 80);
    World.add(world, box1);
    
    runner = Runner.create();
    Runner.run(runner, engine);

    // var options = {
    //   bodyA: node1.body,
    //   bodyB: node2.body,
    //   length: 10,
    //   stiffness: 0.4
    // };
    // var constraint = Constraint.create(options);
    // World.add(world, constraint);
    console.log(world);
  }
  
function draw() {
  // rectMode(CENTER);
  background(250);
  rect(box1.position.x,box1.position.y,80,80);
}
