
// Function to read the JSON from a file - returns a promise containing the parsed JSON
async function readJSONFile(file) {
  // Function will return a new Promise which will resolve or reject based on whether the JSON file is read and parsed successfully
  return new Promise((resolve, reject) => {
      // Define a FileReader Object to read the file
      let fileReader = new FileReader();
      // Specify what the FileReader should do on the successful read of a file
      fileReader.onload = event => {
          // If successfully read, resolve the Promise with JSON parsed contents of the file
          resolve(JSON.parse(event.target.result))
      };
      // If the file is not successfully read, reject with the error
      fileReader.onerror = error => reject(error);
      // Read from the file, which will kick-off the onload or onerror events defined above based on the outcome
      fileReader.readAsText(file);
  });
}

// Function to be triggered when file input changes
async function fileChange(file){
  // As readJSONFile is a promise, it must resolve before the contents can be read - in this case logged to the console
  readJSONFile(file).then(json =>{
    // console.log(json);
    mainSet=new Set([],[]);
    mainSet.importJSON(json);
  })
   
}

function rectangleTool() {
    toolSelected=true;
    currentTool='rectangle';
    //here input the text value
    toggleModal('new');

  }
  function ellipseTool() {
    toolSelected=true;
    currentTool='ellipse';
    //here input the text value
    toggleModal('new');

  }
  function linkTool() {
    //listen to 2 clicks : origin / destination
    toolSelected=true;
    currentTool='link';
  }
  function linkTextTool (link) {
    // let text = prompt("Please enter a text for this link :", "");
    // if(text!=null) link.value=text;
    
  }

  function saveTool(){
    // console.log(path+'.json');
    let text = prompt("Please enter a filename (without extension) :", "");
    let path=text+'.json';
    let saveJSON=mainSet.saveAsJSON();
    localStorage.setItem('save',JSON.stringify(saveJSON));

    // console.log(mainSet.saveAsJSON());
    save(saveJSON,path);

  }

  function loadTool(){}

  function clearTool(){
    const $inputFile = document.querySelector('#file-input');
    $inputFile.value='';
    mainSet=new Set([],[]);

  }

  function openColorPicker(type,shape){
    let $colorPicker=document.querySelector('.color-picker input');
    let object;
    switch(shape){
      case 'rectangle':
        object=menuRectangle.contextTarget;
        break;
      case 'ellipse':
        object=menuEllipse.contextTarget;
        break;
      case 'link':
        object=menuLink.contextTarget;
        
    }
    switch(type){
      case 'stroke':
        $colorPicker.value=object.stroke.color;
        break;
        case 'fill':
        $colorPicker.value=object.fill;
        break;
    }
    $colorPicker.setAttribute('data-type',type);
    $colorPicker.setAttribute('data-shape',shape);
    $colorPicker.focus();
    $colorPicker.click();
  }
  function setColor(color){
    let object;
    let $colorPicker=document.querySelector('.color-picker input');
    let shape=$colorPicker.getAttribute('data-shape');
    let type=$colorPicker.getAttribute('data-type');
  
    switch(shape){
      case 'rectangle':
        object=menuRectangle.contextTarget;
        break;
      case 'ellipse':
        object=menuEllipse.contextTarget;
        break;
      case 'link':
        object=menuLink.contextTarget;

        break;
    }
    if(object!=null){
      console.log(object);
      switch(type){
        case 'stroke':
          console.log(color);
          object.stroke.color=color;
          break;
        case 'fill':
          object.fill=color;
          break;
      }
    }
    // console.log(color);
  }

  function resizeLink(posX,posY){
    let link=menuLink.contextTarget;
    console.log(link);
//     <div class="slidecontainer">
//     <input type="range" min="1" max="100" value="50" class="slider" id="rangeSelector">
//     <div id="output"></div>
// </div>


    var $slider = document.createElement("input");
    $slider.setAttribute('type','range');
    $slider.setAttribute('min',10);
    $slider.setAttribute('max',1000);
    $slider.setAttribute('value',500);
    $slider.classList.add('slider');
    $slider.setAttribute('id','rangeSelector');
    var $output=document.createElement('div');
    $output.setAttribute('id', 'output');
    var $slideContainer=document.createElement('div');
    $slideContainer.classList.add('slidecontainer');
    $slideContainer.appendChild($slider);
    $slideContainer.appendChild($output);
    $slideContainer.style.left=(posX-20)+'px'; 
    $slideContainer.style.top=(posY-20)+'px'; 
    document.getElementById('main').insertBefore($slideContainer, document.getElementById('canvas'));


    // var $slideContainer = document.querySelector('.slidecontainer');
    $slideContainer.classList.add('show');
    var $output = document.getElementById("output");
    $output.innerHTML = $slider.value; // Display the default slider value

    // Update the current slider value (each time you drag the slider handle)
    $slider.oninput = function() {
    $output.innerHTML = this.value;
} 
  }




  