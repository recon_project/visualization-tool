class Rectangle extends Entity {
    constructor(pos,value,options,width,height,stroke,fill,id=0){
        super(pos,value,options);
        this.width=width;
        this.height=height;
        this.stroke=stroke;
        this.fill=fill
        this.body=Bodies.rectangle(this.pos.x,this.pos.y,this.width,this.height,this.options);
        this.anchors=this.buildAnchors();
        this.id=id;
        World.add(world, this.body);
        // have to change this, need our own id for saving to json

    }
    display(){
        if(this.isDragging){
            fill(240);
            stroke(12,323,12);
            strokeWeight(6);
        }else{
            // console.log(this.stroke);
            fill(color(this.fill));
            stroke(color(this.stroke.color));
            strokeWeight(1);
        }
        push();
        var textPos;
        rectMode(CENTER);
        rect(this.body.position.x,this.body.position.y,this.width,this.height);
        textPos=createVector(this.body.position.x,this.body.position.y+4);
        // fill(0);
        textSize(16/currentScale);
        textAlign(CENTER);
        fill(0);
        strokeWeight(0);
        text(this.value,textPos.x,textPos.y);
        // this.pos=this.body.position;
        this.body.position=this.pos;
        pop();
    }
    buildAnchors(){
        let anchors=[];
        let offsetX,offsetY,anchor;

        // 4 corners
        
        offsetX=-this.width/2;
        offsetY=-this.height/2;
        anchor={
            pole:'NW',
            offset:{x:offsetX,y:offsetY}
        }
        anchors.push(anchor);
        
        offsetX=this.width/2;
        offsetY=-this.height/2;
        anchor={
            pole:'NE',
            offset:{x:offsetX,y:offsetY}
        }
        anchors.push(anchor);
        
        offsetX=-this.width/2;
        offsetY=this.height/2;
        anchor={
            pole:'SW',
            offset:{x:offsetX,y:offsetY}
        }
        anchors.push(anchor);

        offsetX=this.width/2;
        offsetY=this.height/2;
        anchor={
            pole:'SE',
            offset:{x:offsetX,y:offsetY}
        }
        anchors.push(anchor);

        // 4 mid sides

        offsetX=0;
        offsetY=-this.height/2;
        anchor={
            pole:'N',
            offset:{x:offsetX,y:offsetY}
        }
        anchors.push(anchor);
        
        offsetX=-this.width/2;
        offsetY=0;
        anchor={
            pole:'W',
            offset:{x:offsetX,y:offsetY}
        }
        anchors.push(anchor);

        offsetX=0;
        offsetY=this.height/2;
        anchor={
            pole:'S',
            offset:{x:offsetX,y:offsetY}
        }
        anchors.push(anchor);

        offsetX=this.width/2;
        offsetY=0;
        anchor={
            pole:'E',
            offset:{x:offsetX,y:offsetY}
        }
        anchors.push(anchor);
        // console.log(anchors);

        return anchors;

    }

    revealAnchors(){
        push();
        rectMode(CENTER);
        fill('white');
        stroke('black');
        let x=this.pos.x;
        let y=this.pos.y;

        this.anchors.forEach(anchor=>{
            let posX=x+anchor.offset.x;
            let posY=y+anchor.offset.y;
            rect(posX,posY,this.anchorSize,this.anchorSize);
        });

        pop();
    
    }
    checkClick(absMouseX, absMouseY, click) {
        let w = this.width * 0.5;
        let h = this.height * 0.5;
        if (absMouseX > this.pos.x - w &&
            absMouseX < this.pos.x + w &&
            absMouseY > this.pos.y - h &&
            absMouseY < this.pos.y + h
        ) {
            if (click == "left") {
                // console.log('clicked on a rectangle : ' + this.value);
                // console.log(this);
                entityDragging = true;
                this.isDragging = true;
                isPanning = false;
            } else if (click == "right") {
                isPanning = false;
                // console.log('clicked right on rect');
                menuRectangle.display(event,this);
            }
            return true;
        }
        return false;
    }
    checkAnchorsClick(absMouseX,absMouseY){
        // expands click zone by 4

        let s=this.anchorSize+4; 
        let anchorX, anchorY;
        let clickedOnAnchor=null;
        let anchorPos=null;

        rectMode(CENTER);

        this.anchors.forEach(anchor => {
            anchorX = this.pos.x + anchor.offset.x;
            anchorY = this.pos.y + anchor.offset.y;
            if (absMouseX > anchorX - s &&
                absMouseX < anchorX + s &&
                absMouseY > anchorY - s &&
                absMouseY < anchorY + s
            ) {
                // console.log('clicked on rect anchor (top-left)');
                clickedOnAnchor = createVector(anchorX, anchorY);
                anchorPos = anchor.pole;
                clickedAnchorPos = clickedOnAnchor;
                if (isLinking && !waitingLinkDest) {
                    clickedLinkSource = true;
                    sourceAnchor = {
                        entityID: this.id,
                        pos: clickedAnchorPos,
                        body: this.body,
                        pole: anchorPos,
                        offset:anchor.offset
                    }
                }
                else if (isLinking && waitingLinkDest) {
                    clickedDestSource = true;
                    destAnchor = {
                        entityID: this.id,
                        pos: clickedAnchorPos,
                        body: this.body,
                        pole: anchorPos,
                        offset:anchor.offset
                    };
                }
                return true;
            }
        });
        return false;

    }
}