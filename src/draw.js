var Engine = Matter.Engine,


  World = Matter.World,
  Bodies = Matter.Bodies,
  Constraint = Matter.Constraint;
  Runner = Matter.Runner;
  Mouse = Matter.Mouse;
  MouseConstraint = Matter.MouseConstraint;

var engine;
var world;
var ground;
var runner;
var mConstraint;
var canvasMouse;
var entities;
var links=[];
var options;
var mainSet, mainSet_temp;

var toolSelected = false;
var inputValue='';
var currentColor;
let currentScale = 1;
const zoomSensitivity = 0.1;


let isMouseDragged = false;
let isPanning, isLinking, entityDragging, clickedLinkSource, waitingLinkDest, clickedDestSource=false;
let clickedAnchorPos=null;
let sourceAnchor,destAnchor=null;
let mousePressedX = null;
let mousePressedY = null;
let absMouseX;
let absMouseY;
let transformX = 0;
let transformY = 0;
const mouseDragDetectionThreshold = 10;

function setup() {
    var mainCanvas=createCanvas(windowWidth, windowHeight);

    mainCanvas.parent("canvas");
    //to prevent the mixup between JS events and P5 canvas events
    mainCanvas.mousePressed(mousePressedAction);
    engine = Engine.create();

    engine.world.gravity.scale=0;

    world = engine.world;
    pos1=createVector(200,450);
    pos2=createVector(400,300);
    pos3=createVector(300,700);
    pos4=createVector(800,500);
    pos5=createVector(900,180);
    pos6=createVector(1050,300);
    pos7=createVector(1750,900);
    pos8=createVector(1750,200);

    options=
        {
            friction:10,
            frictionAir:0.4,
            frictionStatic:0.1,
            restitution:0.1,
            density:10,
            inertia:'Infinity'

        };

        // let stroke = {color:'#000000',type:''}
    node1=new Rectangle(pos1,'Entité 1',options,150,100,{color:'#000000',type:''},'#ffffff',0);
    node2=new Ellipse(pos2,'Entité 2',options,160,{color:'#000000',type:''},'#ffffff',1);
    node3=new Ellipse(pos3,'Entité 3',options,150,{color:'#000000',type:''},'#ffffff',2);
    node4=new Ellipse(pos4,'Entité 4',options,170,{color:'#000000',type:''},'#ffffff',3);
    node5=new Rectangle(pos5,'Entité 5',options,200,100,{color:'#000000',type:''},'#ffffff',4);
    node6=new Ellipse(pos6,'Entité 6',options,130,{color:'#000000',type:''},'#ffffff',5);
    node7=new Ellipse(pos7,'Entité 7',options,120,{color:'#000000',type:''},'#ffffff',6);

    options2= {
      friction:4,
      frictionAir:0.1,
      frictionStatic:0.4,
      restitution:0,
      density:0.1
    }

    // node8=new Entity(pos8,'Entité sans forme 8',options2);

    entities=[node1,node2,node3,node4,node5,node6,node7];
    
    var constraintOptions = [
      {
        node1: node1.id,
        node2: node2.id,
        bodyA: node1.body,
        bodyB: node2.body,
        pointA: {x:0,y:-50},
        pointB: {x:0,y:80},
      length: 250,
      stiffness: 0.005
      },

      {        
        node1: node3.id,
        node2: node1.id,
        bodyA: node3.body,
        bodyB: node1.body,
        pointA: {x:0,y:-75},
        pointB: {x:0,y:50},
        length: 250,
        stiffness: 0.05
      },
      {
        node1: node4.id,
        node2: node5.id,
      bodyA: node4.body,
      bodyB: node5.body,
      pointA: {x:85,y:0},
      pointB: {x:0,y:50},
      length: 250,
      stiffness: 0.01
      },
      {        
        node1: node5.id,
        node2: node6.id,
        bodyA: node5.body,
        bodyB: node6.body,
        pointA: {x:0,y:50},
        pointB: {x:0,y:-65},
        length: 450,
        stiffness: 0.1
      },
      {        
        node1: node7.id,
        node2: node1.id,
        bodyA: node7.body,
        bodyB: node1.body,
        pointA: {x:-60,y:0},
        pointB: {x:50,y:0},
        length: 1050,
        stiffness: 0.01
        }
    ];

    constraintOptions.forEach(optionSet=>{
      var constraint = Constraint.create(optionSet);
      // we need to define the stroke line characteristics here
      let stroke = {
        color:'#94FF00',
        type:'',
        width:'',
        start: '',
        end:''
      }
      var link = new Link(constraint.node1,constraint.node2,constraint,stroke);
      links.push(link);
    });


    
    mainSet_temp=new Set(entities,links);
    // mainSet=new Set([],[]);
    mainSet=mainSet_temp;



var circ = {};
circ.circ = circ;


    
    runner = Runner.create();
    Runner.run(runner, engine);
  }
  
function draw() {
    clear();
    push();
    
    translate(transformX, transformY);
    scale(currentScale);
    
    textFont('cmuTypewriter');
    if(mainSet!=null)
      mainSet.display();

  if (toolSelected) {
    // cursor('../img/rectangle-cur.png');
    cursor('hand');
    currentColor = select('#input-color').elt.value;
    let ncolor = color(currentColor);
    ncolor.setAlpha(122);

    stroke(ncolor);
    noFill();
    absMouseX = (mouseX - transformX) / currentScale;
    absMouseY = (mouseY - transformY) / currentScale;

    switch (currentTool) {
      case 'rectangle':
        rect(absMouseX, absMouseY, 100, 80);
        break;
      case 'ellipse':
        ellipse(absMouseX, absMouseY, 100);
        break;
      case 'link':
        // make appear the anchor points to attach link
        mainSet.seeAnchors();
        if(clickedLinkSource&&!clickedDestSource){
          
          waitingLinkDest=true;
          absMouseX = (mouseX - transformX) / currentScale;
          absMouseY = (mouseY - transformY) / currentScale;
          fill('red');
          rectMode(CENTER);
          rect(clickedAnchorPos.x,clickedAnchorPos.y,13,13);
          stroke(currentColor);
          
          line(clickedAnchorPos.x,clickedAnchorPos.y,absMouseX,absMouseY);
          mainSet.seeAnchors();
       }else if(clickedLinkSource&&clickedDestSource){
          toolSelected=false;
          clickedDestSource=false;
          clickedLinkSource=false;
          isLinking=false;
          waitingLinkDest=false;
          // console.log('dest source clicked');
          // - [x] length maybe should be the current dist between the 2 entities
          // - [x] offset should be addressed relative to clickedAnchorPos
            // - [x] have to integrate the anchors in the entity classes
          // stiffness is set after the link creation (with a right-click menu)
          let sourceVec=sourceAnchor.pos;
          let destVec=destAnchor.pos;
          let distance = dist(sourceVec.x,sourceVec.y,destVec.x,destVec.y);
          // console.log(distance);
          
          let constraintOption=
            {
              bodyA: sourceAnchor.body,
              bodyB: destAnchor.body,
              pointA: sourceAnchor.offset,
              pointB: destAnchor.offset,
              length: distance,
              stiffness: 0.07
            };
          let constraint= Constraint.create(constraintOption);
          // console.log(color(currentColor));
          let stroke = {
            color:color(currentColor),
            type:'',
            weight: ''
          }
          newLink=new Link(sourceAnchor.entityID,destAnchor.entityID,constraint,stroke,'');
          mainSet.addLink(newLink);
       }
        break;
      default:
        break;
    }
  }
    pop();

}
// P5 mouse events
function mousePressedAction() {
  // console.log('mousePressed');
  mousePressedX = mouseX;
  mousePressedY = mouseY;
  absMouseX = (mouseX - transformX)/currentScale;
  absMouseY = (mouseY - transformY)/currentScale;
    if(mouseButton === RIGHT) {
      // console.log('click right mousePressed');
      mainSet.checkMouseClick(absMouseX,absMouseY,'right');
    }
    if(mouseButton === LEFT){

  
      if(toolSelected){
          var newNode;
          var position=createVector(absMouseX,absMouseY);
          let w,h;
          let stroke={
            color:currentColor,
            type:'',
            weight: ''
          }
          switch(currentTool){
            case 'rectangle' :
              toolSelected=false;
              w=inputValue.length*10+20;
              if (w<100) w=100;
              h=100;
             
              newNode=new Rectangle(position,inputValue,options,w,h,stroke,'#00000');
              mainSet.addEntity(newNode)
              break;
              
              case 'ellipse':
              toolSelected=false;
              w=inputValue.length*10+10;
              if (w<100) w=100;
              
              newNode=new Ellipse(position,inputValue,options,w,stroke,'#00000');
              mainSet.addEntity(newNode)
              break;
            case 'link':
              toolSelected=true;
              isLinking=true;
  
              break;
          }
          if(isLinking){
            if(waitingLinkDest){
              // console.log('choose dest point');
  
            }else{
              // console.log('choose source point');
            }
            mainSet.checkAnchorsClick(absMouseX,absMouseY);
          }
  
      } else{
        isPanning=true;
        mainSet.checkMouseClick(absMouseX,absMouseY,'left');
      }
    }

  }
  function mouseDragged() {

    if (dist(mousePressedX, mousePressedY, mouseX, mouseY) > mouseDragDetectionThreshold) {
      if(isPanning){
        isMouseDragged = true;
        currentPos = createVector(transformX,transformY);
        transformX += (mouseX - pmouseX);
        transformY += (mouseY - pmouseY);
        
      }else if(isLinking){
        // isMouseDragged = true;
        //drag the link to another anchor
      
      }else{
        absMouseX = (mouseX - transformX)/currentScale;
        absMouseY = (mouseY - transformY)/currentScale;
        mainSet.updateEntityDragPos(absMouseX,absMouseY);

      }
    }
  }
  function mouseReleased() {
    if (!isMouseDragged) {

    }
    
    mousePressedX = null;
    mousePressedY = null;
    isMouseDragged = false;
    entityDragging=false;
    isLinking = false;
    mainSet.resetDragging();
  }

  function mouseWheel(event) {
    // Determine the scale factor based on zoom sensitivity
    let scaleFactor = null;
    if (event.delta < 0) {
      scaleFactor = 1 + zoomSensitivity;
    } else {
      scaleFactor = 1 - zoomSensitivity;
    }
  
    // Apply transformation and scale incrementally
    currentScale = currentScale * scaleFactor;

    transformX = mouseX - (mouseX * scaleFactor) + (transformX * scaleFactor);
    transformY = mouseY - (mouseY * scaleFactor) + (transformY * scaleFactor);

    return false;
  }


  function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
  }
  
