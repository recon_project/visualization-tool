class Ellipse extends Entity {
    constructor(pos,value,options,diameter,stroke,fill,id=0){
        super(pos,value,options);

        this.diameter=diameter;
        this.stroke=stroke;
        this.fill=fill;
        this.body= Bodies.circle(this.pos.x, this.pos.y, this.diameter/2,this.options);
        this.anchors=this.buildAnchors();
        // console.log(this.anchors);
        World.add(world, this.body);
                // have to change this, need our own id for saving to json
        this.id=id;
    }
    display(){
        if(this.isDragging){
            fill(240);
            stroke(12,323,12);
            strokeWeight(6);
        }else{
            // console.log(this.stroke.color);
            fill(color(this.fill));
            stroke(color(this.stroke.color));
            strokeWeight(1);
        }
        push();
        var textPos;
        ellipseMode(CENTER);
        ellipse(this.body.position.x,this.body.position.y,this.diameter,this.diameter);
        textPos=createVector(this.body.position.x,this.body.position.y+5);

        fill(0);
        textSize(16/currentScale);
        textAlign(CENTER);
        strokeWeight(0);
        text(this.value,textPos.x,textPos.y);
        this.body.position=this.pos;
        pop();
    }
    buildAnchors(){
        let anchors=[];
        let anchorPos;        


        let r=this.diameter/2;
        let angle=PI;
        let pointCount=6;
        for(let i = angle; i < TWO_PI + angle; i += TWO_PI / pointCount){
            let offsetX,offsetY,anchor;
            offsetX=r*Math.cos(i);
            offsetY=r*Math.sin(i);

            let anchorAngle=Math.round(degrees(i)%360);

            switch(anchorAngle){
                case 60:
                    anchorPos='SE';
                    break;
                case 120:
                    anchorPos='SW';
                    break;
                case 180:
                    anchorPos='W';
                    break;
                case 240:
                    anchorPos='NW';
                    break;
                case 300:
                    anchorPos='NE';
                    break;
                case 360:
                    anchorPos='E';
                    break;
                default:
                    break;
            }
            // console.log(offsetX,offsetY);
            anchor={
                pole:anchorPos,
                offset:{x:offsetX,y:offsetY}
            }
            anchors.push(anchor);
        }
        return anchors;
    }
    revealAnchors(){
        // push();
        rectMode(CENTER);
        fill('white');
        stroke('black');
        
        let x=this.pos.x;
        let y=this.pos.y;

        // position points around the circle
        this.anchors.forEach(anchor=>{
            let pointX=x+anchor.offset.x;
            let pointY=y+anchor.offset.y;
            rect(pointX,pointY,this.anchorSize,this.anchorSize);
        });
        
        
        // pop();
    
    }
    checkClick(absMouseX, absMouseY, click) {

        let d = this.diameter * 0.5;
        if (absMouseX > this.pos.x - d &&
            absMouseX < this.pos.x + d &&
            absMouseY > this.pos.y - d &&
            absMouseY < this.pos.y + d
        ) {
            // console.log('clicked on an ellipse : ' + this.value);
            // console.log(this);
            if (click == 'left') {
                entityDragging = true;
                this.isDragging = true;
                isPanning = false;
            } else if (click == 'right') {
                // console.log('clicked right on ellipse');
                menuEllipse.display(event,this);
            }
            return true;
        }
        return false;
    }
    checkAnchorsClick(absMouseX,absMouseY){
        // expands click zone by 4
        let s=this.anchorSize+4;
        
        // position points around the circle
        rectMode('CENTER');
        this.anchors.forEach(anchor=>{
            let pointX,pointY;
            pointX=this.pos.x+anchor.offset.x;
            pointY=this.pos.y+anchor.offset.y;

            if (absMouseX > pointX - s &&
                absMouseX < pointX + s &&
                absMouseY > pointY - s &&
                absMouseY < pointY + s
            ) {
                // console.log('clicked on anchor:');
                clickedAnchorPos=createVector(pointX, pointY);


                // anchorAngle=
                if(isLinking&&!waitingLinkDest){
                    clickedLinkSource=true;
                    sourceAnchor={
                        entityID:this.id,
                        pos:clickedAnchorPos,
                        body:this.body,
                        pole:anchor.pole,
                        offset:anchor.offset
                    }
                }
                else if(isLinking&&waitingLinkDest){
                    clickedDestSource=true;
                    destAnchor={
                        entityID:this.id,
                        pos: clickedAnchorPos,
                        body: this.body,
                        pole:anchor.pole,
                        offset:anchor.offset
                    };
                }
                return true;
            }
        });
        
        return false;
    }
}